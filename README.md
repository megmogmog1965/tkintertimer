## Description

Tkinterの練習に、簡単なタイマーを作ってみる.

## Requirement

* [PyDev] ([Eclipse])
* [Python 2.7.9]

## References

* [Tkinter 8.5 reference: a GUI for Python](http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/index.html)

## Author

[Yusuke Kawatsu]



[Yusuke Kawatsu]:https://bitbucket.org/megmogmog1965
[PyDev]:http://pydev.org/
[Eclipse]:https://eclipse.org/downloads/
[Python 2.7.9]:https://www.python.org/downloads/
