#!/usr/bin/env python
# encoding: utf-8

'''
Created on Feb 27, 2015

Tkinterを使って作った、簡易タイマー.
(Tkinter練習用)

:author: Yusuke Kawatsu
'''

# built-in modules.
import os
import sys
import time
import signal
import atexit
import threading
import datetime
import platform

# Tkinter modules.
import Tkinter as tk
import ttk
import tkFont


_MAX_TIME = 2 * 60 * 60
_SCRIPT_NAME = os.path.basename(sys.argv[0])
_SCRIPT_DIR = os.path.dirname(os.path.abspath(sys.argv[0]))


class App(ttk.Frame):
    '''
    Main frame with Tkiner.
    '''
    
    @classmethod
    def run(cls):
        '''
        run App.
        '''
        root = tk.Tk()
        root.resizable(False, False)
        root.title(_SCRIPT_NAME)
        
        app = App(root)
        atexit.register(app._onExit)
        signal.signal(signal.SIGTERM, app._onExit)
        
        # 全ての終了 (Command+Qとか、Menu->Exitとか) をHandlingする. 
        root.protocol('WM_DELETE_WINDOW', app._onExit)
        root.createcommand('exit', app._onExit) # :see: https://mail.python.org/pipermail/tkinter-discuss/2009-April/001893.html
        
        app.mainloop()
    
    def __init__(self, master=None):
        '''
        Constructor
        '''
        ttk.Frame.__init__(self, master)
        self._master = master
        self._isAlive = True
        self._soundThread = None
        
        # display.
        self._display = ttk.Label(self, text='hello', width=10, anchor=tk.CENTER, font=tkFont.Font(family='Helvetica', size=72, weight='bold'))
        self._display.grid(row=0, column=0, columnspan=1, sticky=tk.W+tk.E)
        
        # scale bar.
        self._scale = ttk.Scale(self, command=self._onScale)
        self._scale.grid(row=1, column=0, columnspan=1, sticky=tk.W+tk.E, padx=10)
        
        self.pack()
        
        # start count down.
        self._startCoundDown()
    
    def _startCoundDown(self):
        '''
        秒読みを開始する
        '''
        # next tick.
        if self._isAlive:
            self.after(1000, self._startCoundDown)
        
        # tick.
        self._tick()
    
    def _tick(self):
        '''
        tick 1 sec.
        '''
        scale = float(self._scale.get())
        if scale <= 0:
            return
        
        # 1秒引いたScale(%)を算出.
        scale = scale - 1.0/_MAX_TIME
        self._scale.set(scale)
        
        if scale <= 0:
            print u'time=0'
            self._soundThread = self._sound()
    
    def _onExit(self):
        '''
        on exit event handler.
        '''
        if not self._isAlive:
            return
        
        try:
            print u'on exit !'
            self._isAlive = False
            if self._soundThread:
                self._soundThread.stop()
            self._master.destroy()
        except:
            pass
    
    def _onScale(self, value):
        '''
        Scale widget event handler.
        '''
        # reflect scale value to display.
        value = float(value)
        current = round(value * _MAX_TIME)
        timeStr = unicode(datetime.timedelta(seconds=current))
        self._display.config(text=timeStr)
    
    @classmethod
    def _sound(cls):
        '''
        :return: 音を鳴らすSafeThread.
        '''
        _innser_soud = None
        
        if platform.system() == u'Darwin':
            # Mac OS X.
            def _sound_impl(stop_event):
                for i in xrange(5):
                    os.system('afplay -v 5 /System/Library/Sounds/Sosumi.aiff')
                    if stop_event.is_set():
                        return
        
        elif platform.system() == u'Windows':
            # Windows.
            # :todo: 動作確認してない
            def _sound_impl(stop_event):
                # :see: http://plaza.rakuten.co.jp/kugutsushi/diary/201010080000/
                import winsound
                for i in range(3):
                    winsound.Beep(440,150)
                    time.sleep(0.85)
                    if stop_event.is_set():
                        return
                winsound.Beep(880,1400)
        
        else:
            # Unix ???
            # :todo: 動作確認してない
            def _sound_impl(stop_event):
                for i in xrange(5):
                    sys.stdout.write('\a')
                    sys.stdout.flush()
                    time.sleep(0.85)
                    if stop_event.is_set():
                        return
        
        soundThread = SafeThread(target=_sound_impl)
        soundThread.start()
        
        return soundThread


class SafeThread(threading.Thread):
    '''
    stop()メソッドを追加して、中断できるようにしたThread.
    '''
    
    def __init__(self, args=(), *nkargs, **kwargs):
        '''
        :param target: Threadで動かすfunction. 第一引数に threading.Event() が来るので、event.is_set() の時にThreadを止める.
        :param args: functionに渡す引数. 第1引数には、 threading.Event() が必ず来る.
        '''
        self._stop_event = threading.Event()
        new_args = [ self._stop_event ]
        new_args.extend(args)
        super(SafeThread, self).__init__(*nkargs, args=new_args, **kwargs)
        
    def stop(self):
        '''
        Threadを停止する.
        '''
        self._stop_event.set()


if __name__ == '__main__':
    App.run()

